# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Albumes,Foto,Profile

class FotoAdmin(admin.ModelAdmin):
    #fields=['fecha_publicacion','descripcion','album','imagen']
    fieldsets = [
        (None,               {'fields': ['descripcion']}),
        ('Información de la Fecha', {'fields': ['fecha_publicacion'],'classes': ['collapse']}),
        ('Datos',{'fields':['album','imagen'],'classes': ['collapse']})
    ]
    list_display = ('descripcion', 'fecha_publicacion','fue_publicada_recientemente')
    list_filter = ['fecha_publicacion']
    search_fields = ['descripcion']

class ProfileAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields':['user']}),
        ('Información Personal',{'fields':['ciudad','fecha_nacimiento'], 'classes':['collapse']})
    ]
    list_display = ('user','ciudad','fecha_nacimiento')
    list_filter = ['fecha_nacimiento']
    search_fields = ['user']

class FotoInline(admin.TabularInline):
    model = Foto
    extra = 2


class AlbumAdmin(admin.ModelAdmin):
    fields=['nombre']
    inlines = [FotoInline]

admin.site.register(Albumes,AlbumAdmin)
admin.site.register(Foto,FotoAdmin)
admin.site.register(Profile,ProfileAdmin)
