# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.utils.encoding import python_2_unicode_compatible
import datetime
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
@python_2_unicode_compatible
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    ciudad = models.CharField(max_length=30, blank=True)
    fecha_nacimiento = models.DateField(null=True, blank=True)
    def __str__(self):
        return self.ciudad

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)    

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()

@python_2_unicode_compatible
class Albumes(models.Model):
    profile = models.ForeignKey(User, related_name='albumes', on_delete=models.CASCADE,null=True)
    nombre = models.CharField(max_length=200)
    def __str__(self):
        return self.nombre
   
@python_2_unicode_compatible
class Foto(models.Model):
    album = models.ForeignKey(Albumes,related_name='fotos',on_delete=models.CASCADE)
    descripcion = models.CharField(max_length = 240)
    imagen = models.TextField()
    fecha_publicacion = models.DateTimeField('Fecha de Publicación')

    def fue_publicada_recientemente(self):
        ahora = timezone.now()
        return ahora - datetime.timedelta(days=1) <=self.fecha_publicacion <= ahora

    fue_publicada_recientemente.admin_order_field = 'fecha_publicacion'
    fue_publicada_recientemente.boolean = True
    fue_publicada_recientemente.short_description = '¿Publicada recientemente?'

    def __str__(self):
        return self.descripcion