from django.contrib.auth.models import User
from .models import Albumes, Foto, Profile
from rest_framework import serializers

class FotoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model= Foto
        fields = ('url','album','descripcion','imagen','fecha_publicacion')

class AlbumSerializer(serializers.HyperlinkedModelSerializer):
    fotos = FotoSerializer(many=True, read_only=True)
    class Meta:
        model = Albumes
        fields = ('url','nombre','profile','fotos')

class UserSerializer(serializers.HyperlinkedModelSerializer):
    ciudad = serializers.CharField(source='profile.ciudad')
    fecha_nacimiento = serializers.DateField(source='profile.fecha_nacimiento')   
    albumes = AlbumSerializer(many=True, read_only=True)
    class Meta:
        model = User
        fields = ('url','username', 'email', 'password','first_name', 'last_name', 'ciudad', 'fecha_nacimiento', 'albumes','is_staff')

    def create(self, validated_data):
        profile_data = validated_data.pop('profile', None)
        user = User.objects.create_user(username=validated_data['username'], email=validated_data['email'], password=validated_data['password'],first_name=validated_data['first_name'],last_name=validated_data['last_name'],is_staff=validated_data['is_staff'])
        user.save()
        self.update_or_create_profile(user, profile_data)
        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile', None)
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()        
        self.update_or_create_profile(instance, profile_data)
        return instance

    def update_or_create_profile(self, user, profile_data):        
        Profile.objects.update_or_create(user=user, defaults=profile_data)





