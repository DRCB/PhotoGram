# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase

# Create your tests here.
import datetime
from django.utils import timezone
from django.urls import reverse

from .models import Foto

class TestDeModeloFoto(TestCase):
    def test_fue_publicada_recientemente_con_una_foto_futura(self):
        """
        fue_publicada_recientemente() returns False para fotos cuya fecha_publicacion
        es en el futuro.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_foto= Foto(fecha_publicacion=time)
        self.assertIs(future_foto.fue_publicada_recientemente(), False)
    
    def test_fue_publicada_recientemente_con_fotos_viejas(self):
        """
        fue_publicada_recientemente() returns False para fotos cuya fecha_publicacion
        es mayor que 1 dia.
        """
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_foto = Foto(fecha_publicacion=time)
        self.assertIs(old_foto.fue_publicada_recientemente(), False)

    def test_fue_publicada_recientemente_con_foto_reciente(self):
        """
        fue_publicada_recientemente() returns True para fotos cuya fecha_publicacion
        esta dentro del ultimo dia.
        """
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_foto = Foto(fecha_publicacion=time)
        self.assertIs(recent_foto.fue_publicada_recientemente(), True)

def create_foto(descripcion, days):
    """
    Crea una Foto con la descripcion ingresada y la fecha de publicacion dada por el numero en 'dias` 
    offset to now (negativo para fotos publicadas en el pasado, positivo para fotos que aún no se 
    han publicado).
    """
    time = timezone.now() + datetime.timedelta(days=days)
    return Foto.objects.create(descripcion=descripcion, fecha_publicacion=time)


class FotoIndexViewTests(TestCase):
    def test_no_hay_fotos(self):
        """
        Si no existen fotos, un mensaje apropiado se muestra.
        """
        response = self.client.get(reverse('photos:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No hay fotos disponibles.")
        self.assertQuerysetEqual(response.context['lista_ultimas_fotos'], [])

    def test_foto_antigua(self):
        """
        Preguntas con una fecha_publicacion en el pasado se muestran en el index.
        """
        create_foto(descripcion="Foto Antigua.", days=-30)
        response = self.client.get(reverse('photos:index'))
        self.assertQuerysetEqual(
            response.context['lista_ultimas_fotos'],
            ['<Foto: Foto Antigua.>']
        )

    def test_foto_futura(self):
        """
        Fotos con una fecha_publicacion en el futuro no se muestran en el index.
        """
        create_foto(descripcion="Foto en el futuro.", days=30)
        response = self.client.get(reverse('photos:index'))
        self.assertContains(response, "No hay fotos disponibles.")
        self.assertQuerysetEqual(response.context['lista_ultimas_fotos'], [])

    def test_foto_futura_y_foto_antigua(self):
        """
        Even if both past and future questions exist, only past questions
        are displayed.
        """
        create_foto(descripcion="Foto Antigua.", days=-30)
        create_foto(question_text="Foto en el futuro.", days=30)
        response = self.client.get(reverse('photos:index'))
        self.assertQuerysetEqual(
            response.context['lista_ultimas_fotos'],
            ['<Foto: Foto Antigua.>']
        )

    def test_dos_fotos_antiguas(self):
        """
        La pagina Index debe poder mostrar varias fotos.
        """
        create_foto(descripcion="Foto Antigua 1.", days=-30)
        create_foto(descripcion="Foto Antigua 2.", days=-5)
        response = self.client.get(reverse('photos:index'))
        self.assertQuerysetEqual(
            response.context['lista_ultimas_fotos'],
            ['<Foto: Foto Antigua 2.>', '<Foto: Foto Antigua 1.>']
        )


