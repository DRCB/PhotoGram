from django.conf.urls import url

from . import views

app_name = 'photos'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    # ex: /albumes/
    url(r'^albumes/$', views.albumes, name='albumes'),
    # ex: /albumes/5/
    url(r'^albumes/(?P<album_id>[a-zA-Z]+)/$', views.album, name='album'),
    #ex: /crear_album/
    url(r'^crear_album/$', views.crear_album, name='crear_album'),
    # ex: /albumes/5/fotos/3
    url(r'^albumes/(?P<album_id>[a-zA-Z]+)/fotos/(?P<pk>[0-9]+)/$', views.FotoView.as_view(), name='foto'),
    url(r'^registro',views.crear_perfil,name='crear_perfil'),
    #url(r'^login', views.login,name='login')
]