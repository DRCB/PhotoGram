# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.core.urlresolvers import reverse_lazy

from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_401_UNAUTHORIZED
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from .serializers import UserSerializer, AlbumSerializer, FotoSerializer
from .models import Foto, Albumes

class IndexView(generic.ListView):
    template_name = 'photos/index.html'
    context_object_name = 'lista_ultimas_fotos'
    def get_queryset(self):
         return Foto.objects.filter(fecha_publicacion__lte=timezone.now()).order_by('-fecha_publicacion')[:5]
 
def albumes(request):
    albumes_usuario= Albumes.objects.all()
    context = {'albumes_usuario':albumes_usuario}
    return render(request,'photos/albumes.html',context)

def album(request,album_id): 
    return HttpResponse("Estas en el album %s." % album_id)

def crear_album(request):
    albumes_usuario= Albumes.objects.all()

    if not request.POST['album']:
        # Redisplay the question voting form.
        return render(request, 'photos/albumes.html', {
            'albumes_usuario':albumes_usuario,
            'error_message': "Debes Ingresar un nombre para el album.",
        })
    else:        
        nuevo_album = Albumes(nombre=request.POST['album'])
        nuevo_album.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('photos:albumes', args=()))

class FotoView(generic.DetailView):
    model = Foto
    template_name = 'photos/foto.html'
    def get_queryset(self):
        """
        Excluye cualquier foto que no se haya publicado todavia.
        """
        return Foto.objects.filter(fecha_publicacion__lte=timezone.now())

def crear_perfil(request):
    
    if not request.POST.get('nombreusuario'):
        # Redisplay the question voting form.
        return render(request, 'photos/registro.html', {
            'error_message': "Debes Ingresar un nombre de usuario.",
        })
    else:
        user = User.objects.create_user(username=request.POST.get('nombreusuario'), email=request.POST.get('correo'), password=request.POST.get('password'))       
        user.profile.ciudad = request.POST['ciudad']
        user.profile.fecha_nacimiento = request.POST['fecha_nacimiento']
        user.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('photos:crear_perfil', args=()))

@api_view(["POST"])
def login(request):
    username = request.data.get("username")
    password = request.data.get("password")

    user = authenticate(username = username,password = password)
    if not user:
        return Response({'error':'Fallo el inicio de Sesion'},status=HTTP_401_UNAUTHORIZED)
    token, _ = Token.objects.get_or_create(user=user)
    return Response({"usuario":user.pk,"token": token.key})

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    #permission_classes = (IsAuthenticated,)

class AlbumViewSet(viewsets.ModelViewSet):
    queryset = Albumes.objects.all()
    serializer_class = AlbumSerializer
    permission_classes = (IsAuthenticated,)

class FotoViewSet(viewsets.ModelViewSet):
    queryset = Foto.objects.all()
    serializer_class = FotoSerializer
    permission_classes = (IsAuthenticated,)
